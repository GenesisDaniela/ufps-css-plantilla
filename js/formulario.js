/* JS Document
 * author: @Jhonny_Guarin @GenesisDaniela @DanielaSanchezb
 * Version: 1
 * Modificado por :
 */

const datos = document.getElementsByName("datos");
/*
* campoEmail valida que el campo contenga el carácter @, precedido y postergado de este debe tener texto.
* Implementa una expresión regular para validar el campo de tipo email
*/
function campoEmail(e){
    const validarEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    for (const elemento of datos) {
        if (elemento.getAttribute("type") === "email") {
            if (validarEmail.test(elemento.value)) {
                document.getElementById('error_email').classList.replace('form_input_error_activo',
                    'form_input_error')
                return true;
            }
            else{
                document.getElementById('error_email').classList.replace('form_input_error',
                    'form_input_error_activo')
                return false;
            }
        }
    }
}
/*
* soloLetras valida que el campo solo admita letras sin acento y el caracter espacio.
*/
function soloLetras(e){
    var key = window.event ? e.which : e.keyCode;
    if ((key >= 65 && key <= 90) || (key >= 97 && key<=122) ||
        (key == 32)){
        document.getElementById('error_texto').classList.replace('form_input_error_activo',
            'form_input_error')
    }
    else{
        e.preventDefault();
        document.getElementById('error_texto').classList.replace('form_input_error',
            'form_input_error_activo')
    }
}
/*
* soloNumeros valida que el campo solo admita numeros en un determinado rango usadno min y max.
*/
function soloNumeros(e){
    for (const elemento of datos){
        if (elemento.getAttribute("type") === "number") {
            let valor = parseFloat(elemento.value);
            let min = parseInt(elemento.getAttribute("min"));
            let max = parseInt(elemento.getAttribute("max"));
            if (valor >= min && valor <= max) {
                document.getElementById('error_numero').classList.replace('form_input_error_activo',
                    'form_input_error')
                return true;
            }
            else{
                document.getElementById('error_numero').classList.replace('form_input_error',
                    'form_input_error_activo')
                return false;
            }
        }
    }
}
/*
* boton realiza las validaciones correspondientes para poder enviar la información.
*/
function boton(){
    validaCheckbox();
    validaFecha();
}

/*
* validaCheckbox valida que no esté seleccionado por lo menos una casilla.
*/
function validaCheckbox() {
    let checked1 = document.getElementById('checkbox1').checked;
    let checked2 = document.getElementById('checkbox2').checked;
    if (checked1== false && checked2==false) {
       document.getElementById('error_txtCheckbox').classList.replace('form_input_error','form_input_error_activo');
    } else{
        document.getElementById('error_txtCheckbox').classList.replace('form_input_error_activo','form_input_error');
    }
}
/*
* validaFecha valida que el campo fecha no sea nulo.
*/
function validaFecha(){
    let fecha = document.getElementById("fecha").value;
    if(fecha=="") {
        document.getElementById("error_fecha").classList.replace('form_input_error','form_input_error_activo');
    } else{
        document.getElementById("error_fecha").classList.replace('form_input_error_activo','form_input_error');
    }
}

window.addEventListener("load", function() {
    document.getElementById('checkbox1').addEventListener("change", validaCheckbox, false);
    document.getElementById('soloLetras').addEventListener("keypress", soloLetras, false);
    document.getElementById('soloNumeros').addEventListener("keyup", soloNumeros, false);
    document.getElementById('campoEmail').addEventListener("keyup", campoEmail, false);

});

