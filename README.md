![Banner](http://www.madarme.co/portada-web.png)

# Título del proyecto

#### Plantilla UFPS-CSS

***

## Índice 
1. 🧾 [Características](#características)
2. 📝 [Contenido del proyecto](#contenido-del-proyecto)
3. 💻 [Tecnologías](#tecnologías)
4. 🖥 [IDE](#ide)
5. 💾 [Instalación](#instalacion)
6. 🎞 [Demo](#demo)
7. 👥 [Autor(es)](#autores)
8. 🏛 [Institución Académica](#institución-académica)
8. 🔍 [Referencias](#referencias)

***

#### Características
- Diseño de un sitio usando una hoja de estilo denominada ![CSS-UFPS](https://img.shields.io/badge/CSS-UFPS-red). El proyecto contiene plantillas de ejemplo para la creación de menús, formularios, galeria y enmaquetado general de una página web.

- Diseño implentado con [![HTML5](https://img.shields.io/badge/HTML5-red)](#tecnologías) , [![CSS](https://img.shields.io/badge/CSS-red)](#tecnologías) y [![CSS grid](https://img.shields.io/badge/CSS-grid-red)](#tecnologías).

- Los fromularios fueron relizados usando el lenguaje [![JavaScript](https://img.shields.io/badge/JavaScript-red)](#tecnologías).

***

#### Contenido del proyecto
| Archivo      | Descripción  |
|--------------|--------------|
|[index.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/index.html) | Archivo principal del proyecto para ejecutar el sitio. |
| [css/formularios.css](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/css/formularios.css) | Archivo CSS implementado para los estilos de los formularios. |
| [css/menu.css](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/css/menu.css) | Archivo CSS implementado para los estilos de los menús. |
| [css/ufps-css.css](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/css/ufps-css.css) | Archivo CSS implementado para los estilos del sitio. |
| [html/formulario1.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/html/formulario1.html) | Archivo HTML implementado para el ejemplo del primer formulario. |
| [html/formulario2.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/html/formulario2.html) | Archivo HTML implementado para el ejemplo del segundo formulario. |
| [html/funcionalidades.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/html/funcionalidades.html) | Archivo HTML implementado para el ejemplo de enmaquetado de un sitio. |
| [html/galeria.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/html/galeria.html) | Archivo HTML implementado para el ejemplo del manejo de imagenes y videos. |
| [html/plantilla_base.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/html/plantilla_base.html) | Archivo HTML implementado para la plantilla base del sitio. |
| [html/tablas.html](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/html/tablas.html) | Archivo HTML implementado para el ejemplo del manejo de tablas. |
| [img](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/tree/master/img) | Carpeta contenedora de documentos visuales del sitio. |
| [img/formularios](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/tree/master/img/formularios) | Carpeta contenedora de documentos visuales para los formularios. |
| [img/galeria](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/tree/master/img/galeria) | Carpeta contenedora de documentos visuales para el ejemplo de galeria. |
| [img/index](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/tree/master/img/index) | Carpeta contenedora de documentos visuales para el index del sitio. |
| [js/formulario.js](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/blob/master/js/formulario.js) | Archivo JAVASCRIPT para la funcionalidad de los formularios. |

***

#### Tecnologías

| Tecnología      | Definición  |
|--------------|--------------|
|[![HTML5](https://img.shields.io/badge/HTML5-red)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5) | HTML5 (HyperText Markup Language, versión 5) es la quinta revisión del lenguaje HTML. Esta nueva versión (aún en desarrollo), y en conjunto con CSS3, define los nuevos estándares de desarrollo web, rediseñando el código para resolver problemas y actualizándolo así a nuevas necesidades. No se limita solo a crear nuevas etiquetas o atributos, sino que incorpora muchas características nuevas y proporciona una plataforma de desarrollo de complejas aplicaciones web (mediante los APIs). [(Garro, A. (s. f.).)](#garro-a-s-f-qué-es-html5-html5-arkaitzgarro-recuperado-de-httpswwwarkaitzgarrocomhtml5capitulo-1html) . |
| [![CSS](https://img.shields.io/badge/CSS-red)](https://developer.mozilla.org/es/docs/Web/CSS) |CSS (en inglés Cascading Style Sheets) es lo que se denomina lenguaje de hojas de estilo en cascada y se usa para estilizar elementos escritos en un lenguaje de marcado como HTML. CSS separa el contenido de la representación visual del sitio. CSS fue desarrollado por W3C (World Wide Web Consortium) en 1996 por una razón muy sencilla. HTML no fue diseñado para tener etiquetas que ayuden a formatear la página. Está hecho solo para escribir el marcado para el sitio. [(B., G. (2019, 13 mayo)) . ](#b-g-2019-13-mayo-qué-es-css-tutoriales-hostinger-recuperado-de-httpswwwhostingercotutorialesque-es-css)|
| [![CSS grid](https://img.shields.io/badge/CSS-grid-red)](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Grid_Layout) |CSS Grid layout contiene funciones de diseño dirigidas a los desarrolladores de aplicaciones web. El CSS grid se puede utilizar para lograr muchos diseños diferentes. También se destaca por permitir dividir una página en áreas o regiones principales, por definir la relación en términos de tamaño, posición y capas entre partes de un control construido a partir de primitivas HTML. [(MDN. (2021, 1 junio)) . ](#css-grid-layout-css-mdn-2021-1-junio-mdn-web-docs-recuperado-de-httpsdevelopermozillaorgesdocswebcsscss_grid_layout)|
| [![JavaScript](https://img.shields.io/badge/JavaScript-red)](https://developer.mozilla.org/es/docs/Web/JavaScript) |JavaScript (JS) es un lenguaje de programación ligero, interpretado, o compilado justo-a-tiempo (just-in-time) con funciones de primera clase. Si bien es más conocido como un lenguaje de scripting (secuencias de comandos) para páginas web, y es usado en muchos entornos fuera del navegador, tal como Node.js, Apache CouchDB y Adobe Acrobat. JavaScript es un lenguaje de programación basada en prototipos, multiparadigma, de un solo hilo, dinámico, con soporte para programación orientada a objetos, imperativa y declarativa (por ejemplo programación funcional). Lee más en acerca de JavaScript. [(MDN. (2021, 1 junio)) . ](#javascript-mdn-2021-1-junio-mdn-web-docs-recuperado-de-httpsdevelopermozillaorgesdocswebjavascript)|

***

#### IDE
- [![Sublime Text 3](https://img.shields.io/badge/SublimeText3-red)](https://www.sublimetext.com/download)

El proyecto Plantilla UFPS-CSS se dearrolló utilizando la IDE Sublime Text 3. Sublime Text es un editor de Texto para escribir código en casi cualquier formato de archivo. Está especialmente pensado para escribir sin distracciones. Esto quiere decir que visualmente ofrece un entorno oscuro donde las líneas de código que escribas resaltarán para que puedas centrarte exclusivamente en ellas. La versión actual es Sublime Text 3, y está disponible para macOS, Windows, y Linux. [(Ferré, A. (2019, 5 noviembre))](#ferré-a-2019-5-noviembre-sublime-text-información-y-trucos-para-empezar-desde-cero-cipsa-academia-cursos-informática-en-barcelona-y-bilbao-recuperado-de-httpscipsanetsublime-text-informacion-y-trucos-para-empezar-desde-cero7etextsublime20text20es20un20editorpensado20para20escribir20sin20distraccionestextla20versic3b3n20actual20es20sublimemacos2c20windows2c20y20linux) .

<div align="center">
  <img width="100" height="120" src="http://tomasdelvechio.github.io/img/blog/2018/logo-sublime-text-3.png">
</div>

***

#### Instalación

1. ![Local](https://img.shields.io/badge/Local-red)
   - Para la instalacion local del proyecto mediante descarga, puede hacerlo por el siguiente link: [Descargar](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/archive/master/ufps-css-plantilla-master.zip)
   - Dirijase a su carpeta de **Descargas** en su equipo
   - Descomprima el archivo **.zip** llamado **ufps-css-plantilla-master**
   - Entre a la carpeta descomprimida y situe el archivo **index.html**, ejecutelo mediante su navegador predeterminado haciendo doble click en el archivo.
2. ![GitLab](https://img.shields.io/badge/GitLab-red)
   - Realizar un Fork [(Reaizar Fork)](https://gitlab.com/GenesisDaniela/ufps-css-plantilla/-/forks/new). Esto le permitira tener un copia del repositorio en su cuenta propia. 
   - Clonar el proyecto. Al clonar el proyecto en su IDE de preferencia podrá editarlo y modificarlo a su gusto. O bien puede realizar una copia Local de su nuevo repositorio. 
   
***

#### Demo

|Para poder visualizar la demo del aplicativo puede dirigirse a: | [UFPS-CSS-Plantilla](https://genesisdaniela.gitlab.io/ufps-css-plantilla)|
|--------------|--------------|

En el anterior link podrá ver una versión de prueba del proyecto aún **NO finalizado**. **Versión**: [![Version](https://img.shields.io/badge/1.3-gray)](#)

***

#### Autor(es)
Proyecto desarrollado por:
- [Jhonny Esneider Guarin Chavez] (<jhonnyesneidergcha@ufps.edu.co>).
- [Daniela Juliana Sanchez Bayona] (<danielajulianasbay@ufps.edu.co>).
- [Genesis Daniela Vargas Jauregui] (<genesisdanielavjau@ufps.edu.co>).

   [Jhonny Esneider Guarin Chavez]: <https://gitlab.com/Jhonny_Guarin>
   [Daniela Juliana Sanchez Bayona]: <https://gitlab.com/DanielaSanchezb>
   [Genesis Daniela Vargas Jauregui]: <https://gitlab.com/GenesisDaniela>
   
****

#### Institución Académica

Proyecto desarrollado en la Materia Programación Web del [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]

   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
 
 <div align="center">
   <img src="https://ingsistemas.cloud.ufps.edu.co/rsc/img/logo_vertical_ingsistemas_ht180.png" width="400" height="130">
</div>

***
#### Referencias

##### Garro, A. (s. f.). _¿Qué es HTML5?_ | HTML5. Arkaitzgarro. Recuperado de https://www.arkaitzgarro.com/html5/capitulo-1.html

##### B., G. (2019, 13 mayo). _¿Qué es CSS? Tutoriales Hostinger_. Recuperado de https://www.hostinger.co/tutoriales/que-es-css

##### CSS Grid Layout - CSS | MDN. (2021, 1 junio). MDN Web Docs. Recuperado de https://developer.mozilla.org/es/docs/Web/CSS/CSS_Grid_Layout

##### Ferré, A. (2019, 5 noviembre). Sublime Text: Información y trucos para empezar desde cero. CIPSA Academia Cursos Informática en Barcelona y Bilbao. Recuperado de https://cipsa.net/sublime-text-informacion-y-trucos-para-empezar-desde-cero/#:%7E:text=Sublime%20Text%20es%20un%20editor,pensado%20para%20escribir%20sin%20distracciones.&text=La%20versi%C3%B3n%20actual%20es%20Sublime,macOS%2C%20Windows%2C%20y%20Linux.

##### JavaScript | MDN. (2021, 1 junio). MDN Web Docs. Recuperado de https://developer.mozilla.org/es/docs/Web/JavaScript
